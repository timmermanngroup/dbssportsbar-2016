<?php
/**
 * Default config settings
 *
 * Enter any WordPress config settings that are default to all environments
 * in this file. These can then be overridden in the environment config files.
 * 
 * Please note if you add constants in this file (i.e. define statements) 
 * these cannot be overridden in environment config files.
 * 
 * @package    Studio 24 WordPress Multi-Environment Config
 * @version    1.0
 * @author     Studio 24 Ltd  <info@studio24.net>
 */
  

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'q|U(>=Bs=e9X9JSWO-8^5G[Mke#ZVG{LkrIj1QmZ[bEed?g4Q73m~li@)$e)Rb,6');
define('SECURE_AUTH_KEY',  '5mW<,u|e!!9hoaM9a/@{,}FNys[1Ke;pTfI>LkSs{//zb8|(,p|J[+a/jL^c%y7m');
define('LOGGED_IN_KEY',    's%Jg.s<E]|~=E@*K;AnFdZ$=w9.n}hH+xnblTuV(h;B=^#Zjsc&7U5s|Xe,%Ych[');
define('NONCE_KEY',        '1{h+3dXi-]pRC8i)/csb3>Kf2`.pVp[CYZ~9LB-mkPPT_*n{Q,Rh{^-Dz?/=WuTc');
define('AUTH_SALT',        'Fo(0+H+OKhCh|yL-V 4N%7e?jZBNPnbj*Mt$(6+aKVb#|8is`s)aWUBQ{D9(w:-H');
define('SECURE_AUTH_SALT', 'NM-Tp+Aso.?^=Y/>/xRyV||C9{>T]5P 1^;;;8gvgyG!8.({rVX5t-ww3o|1h-P>');
define('LOGGED_IN_SALT',   'pEb*,y=,]g#tZtF95ibv`uoA`+E1+2<jVA*(rG+o#3hy(3@jc*!JHd>f#~(+x|!D');
define('NONCE_SALT',       'GPgwSY#dR#c+5N5-a@J64Ouz`?$ o))&J3M|oH[V;7gG:iAZ;y&]K[B-H8HX]yJ#');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * Increase memory limit. 
 */
define('WP_MEMORY_LIMIT', '64M');