//Add your Google Analytics Events below
//ga('send', 'event', [eventCategory], [eventAction], [eventLabel], [eventValue], [fieldsObject]);

jQuery(document).ready( function($) {
    
    const debug = false;
    let eventBuilder = new EventBuilder(debug);
    
    /* # Forms 
    ================================================== */
    eventBuilder.bind('form:not(#gform_3)');
    
    /* # General Contact 
    ================================================== */
    eventBuilder.bind('a[href^="tel:"], a[href^="mailto:"]');
    
    /* # Address
    ================================================== */
    eventBuilder.bind('a.ga-address');
    
    /* # Email Sign Ups
    ================================================== */
    eventBuilder.bind('form#gform_3', { 
        category: "Engagement",
        action: "Submit", 
        label: "Newsletter Sign Up"
    });

    var eventRegistration = { 
        category: "Engagement",
        action: "Click", 
        beforeLabel: "Visit Event Link | "
    }
    // Content Links
    var contentEventRegistration = Object.create(eventRegistration);
    contentEventRegistration.label = function(e) { 
        return $(e).text();
    }
    eventBuilder.bind(".event__content a:not(.event__button)", contentEventRegistration)

    // Event Button Links

    var linkEventRegistration = Object.create(eventRegistration);
    linkEventRegistration.label = function(e) { 
        return $(e).data('title');
    }
    eventBuilder.bind(".event__button", linkEventRegistration)

});