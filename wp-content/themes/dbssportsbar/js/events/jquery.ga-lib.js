/*
 * This is a Google Analytics Plugin developed
 * by Timmermann Group to help with sending
 * Google Events.
*/

class Event { 
    constructor(category, action, label, beforeLabel = "", afterLabel = "", debug = false) {
        this.category = category;
        this.action = action;
        this.bindAction = this.action.toLowerCase();
        this.label = this.trim(label);
        this.debug = debug;
        this.beforeLabel = beforeLabel;
        this.afterLabel = afterLabel;
    }
    
    trim(string) { 
        return string.replace(/\s+/g,' ').trim();
    }

    send() { 
        if(this.debug == true) {
            console.log(this);
        } else { 
            if(window.hasOwnProperty("ga")) {
                ga('send', 'event', this.category, this.action, this.beforeLabel + this.label + this.afterLabel);
            } else { 
                console.error("Unable to send event; no ga loaded");
                console.error(this);
            }
        }
    }
}

class FormEnumerator { 

    constructor() { 
        var forms = {};

        $(".gform_wrapper form").each(function() { 
            let title = $(this).find(".ga-form").first().find("input[type=hidden]").first().val();
            let id = $(this).attr("id").split("_")[1];
            forms[id] = title;
        })

        this.forms = forms;
        this.formEvents = {};

        this.watchAjaxForms();
    }

    watchAjaxForms() { 
        let self = this;
        $(document).on("gform_confirmation_loaded", function(e, form_id) {
            let event = self.formEvents[form_id];
            if(event) { 
                event.send();
            }
        });
    }

    bindAjaxEvent(element, event) { 
        let id = $(element).attr("id").split("_")[1];
        this.formEvents[id] = event;
    }
}


class EventBuilder { 

    constructor(debug = false) { 
        this.debug = debug;   

        if(debug === false) { 
            if(!window.hasOwnProperty("ga")) {
                console.warn("Debug is disabled but ga not found. Events will not send.");
            }
        }

        this.formEnumerator = new FormEnumerator();
    }

    bind(selector, options) { 
        let self = this;

        var $selector;
        if (selector instanceof jQuery) { 
            $selector = selector;
        } else { 
            $selector = $(selector);
        }

        if(this.debug == true) { 
            console.log(`Adding ${selector}; ${ $selector.length} match(es)`);
        }

        $selector.each(function() { 
            let event = self.generate(this, options);
            if(event.bindAction == "gform_confirmation_loaded") { 
                self.formEnumerator.bindAjaxEvent(this, event);
            } else { 
                $(this).on(event.bindAction.toLowerCase(), function() {
                    event.send();
                });
            }
        });
    }

    dynamicBind(selector, dynamicEvent, options) { 
        let self = this;

        var selector;
        if (selector instanceof jQuery) { 
            selector = selector.selector;
        } else { 
            selector = selector;
        }

        if(this.debug == true) { 
            console.log(`(Dynamic) Watching ${selector} for ${dynamicEvent}`);
        }

        $('body').on(dynamicEvent.toLowerCase(), selector, function(e) {
            var bindingEvent = self.getBindingAction($(this), options);

            if(bindingEvent == "gform_confirmation_loaded") { 
                console.warn("AJAX Forms Cannot be Dynamically Binded Properly - This may result in missed events.");
            }

            self.generate(this, options).send();
        });
    }

    generate(selector, options) { 
        var element;
        if (selector instanceof jQuery) { 
            element = selector;
        } else { 
            element = $(selector);
        }

        let beforeLabel = this.getBeforeLabel( element, options );
        let label = this.getLabel( element, options );
        let afterLabel = this.getAfterLabel( element, options );
        let category = this.getCategory( element, options );
        let action = this.getAction( element, options );
        let bindAction = this.getBindingAction( element, options );

        var event = new Event(category, action, label, beforeLabel, afterLabel, this.debug); 
        event.bindAction = bindAction

        return event;
    }

    getBeforeLabel( element, args ) {
        if( element.data( 'before-label' ) ) {
            return element.data( 'before-label' );
        } else if( args && args.beforeLabel ) {
            if(typeof args.beforeLabel == "function") { 
                return args.beforeLabel(element) || "";
            } else { 
                return args.beforeLabel || "";
            }
        } else if( element.is( 'form' ) ) {
            return 'Form Submitted | ';
        } else if( element.is( 'a[href^="tel:"]' ) ) {
            return 'Phone Number Clicked | ';
        } else if( element.is( 'a[href^="mailto:"]' ) ) {
            return 'Email Address Clicked | ';
        } else if( element.is( '.ga-address' ) ) {
            return 'Address Clicked | ';
        } else {
            return '';
        }
    }

    getLabel( element, args ) {
        if( element.data( 'label' ) ) {
            return element.data( 'label' );
        } else if( args && args.label ) {
            if(typeof args.label == "function") { 
                return args.label(element) || "";
            } else { 
                return args.label || "";
            }
        } else if( element.is( 'form' ) ) {
            if( element.find('.ga-form input[type="hidden"]').val() ) {
                return element.find('.ga-form input[type="hidden"]').val();
            } else { 
                return "Form";
            }
        } else {
            return element.text();
        }
    }
    
    getAfterLabel( element, args ) {
        if( element.data( 'after-label' ) ) {
            return element.data( 'after-label' );
        } else if( args && args.afterLabel ) {
            if(typeof args.afterLabel == "function") { 
                return args.afterLabel(element) || "";
            } else { 
                return args.afterLabel || "";
            }
        } else if( element.hasClass( 'ga-download' ) ) {
            //.. else if the element is a mailto link..
            return ' | Downloaded';
        } else {
            return '';
        }
    }
    
    getCategory( element, args ) {
        if( element.data( 'category' ) ) {
            return element.data( 'category' );
        } else if( args && args.category ) {
            return args.category; 
        } else if( element.is( 'a[href^="tel:"]' ) || element.is( 'a[href^="mailto:"]' ) || element.is( 'form' ) ) {
            return 'Contact';
        } else if( element.hasClass( 'ga-download' ) ) {
            return 'Downloads';
        } else {
            return 'Engagement';
        }
    }

    getAction( element, args ) {
        if( element.data( 'action' ) ) {
            return element.data( 'action' )
        } else if( args && args.action ) {
            return args.action;
        } else if( element.is( 'form' ) ) {
            return 'Submit';
        } else if( element.hasClass( 'ga-download' ) ) {
            return 'Download';
        } else {
            return 'Click';
        }
    }

    begins(str, word) {
        return str.lastIndexOf(word, 0) === 0;
    }    

    getBindingAction(element) { 
        if(!element.is("form")) { 
            return this.getAction(element);
        }

        if(element.attr("target") && this.begins(element.attr("target"), "gform_ajax")) { 
            return "gform_confirmation_loaded";
        } else { 
            return "submit";
        }
    }
    
}