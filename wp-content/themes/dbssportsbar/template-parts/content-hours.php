<table class="hours--table">
    <tr>
        <td>
            Sunday
        </td>
        <td>
            <?php the_field('hours_sunday', 'option') ?>   
        </td>
    </tr>
    <tr>
        <td>
            Monday
        </td>
        <td>
        <?php the_field('hours_monday', 'option') ?>
        </td>
    </tr>
    <tr>
        <td>
        Tuesday
        </td>
        <td>
        <?php the_field('hours_tuesday', 'option') ?>
        </td>
    </tr>
    <tr>
        <td>
        Wednesday
        </td>
        <td>
        <?php the_field('hours_wednesday', 'option') ?>
        </td>
    </tr>
    <tr>
        <td>
        Thursday
        </td>
        <td>
        <?php the_field('hours_thursday', 'option') ?>
        </td>
    </tr>
    <tr>
        <td>
        Friday
        </td>
        <td>
        <?php the_field('hours_friday', 'option') ?>
        </td>
    </tr>
    <tr>
        <td>
        Saturday
        </td>
        <td>
        <?php the_field('hours_saturday', 'option') ?>
        </td>
    </tr>
</table>