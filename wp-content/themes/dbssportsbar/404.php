<?php
/**
 * Template Name: Contact Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dbssportsbar
 */

get_header(); ?>

<!-- content -->    
  <div class="container interior-content">
	<div class="row">
		<div class="col-md-12">
			<h1>Oops! That page can't be found.</h1>
		</div>
	</div>
  </div>


<?php get_footer(); ?>