<?php
/**
 * Template Name: Menu Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package dbssportsbar
 */

get_header(); ?>

<!-- content -->    
  <div class="container interior-content">
    <div class="row">

      <div class="col-sm-3 category-list hidden-xs">
      <h1>MENU</h1>
        <?php $categories = get_categories(); ?>
        <?php foreach ($categories as $category):?>
          <a href="#<?php echo $category->slug; ?>"><?php echo $category->name;?></a>
        <?php endforeach; ?>
      </div>
      <div class="col-sm-3"></div>

      <div class="col-sm-9 menu-items">
        <?php $terms = get_terms( 'category' ); ?>
        <?php foreach($terms as $term): ?>
          <h3 id = "<?php echo $term->slug; ?>"><?php echo $term->name; ?>
          <span class="category-price"><?php the_field('category_price', $term); ?><span></h5>
          <h4><?php echo $term->description; ?></h4>
         <?php $term_id = $term->slug; $args = array('post_type'=>'menu', 'posts_per_page'=>9999, 'tax_query'=>array(array('taxonomy'=>'category', 'field'=>'slug', 'terms'=>$term_id))); ?>
          <?php $loop = new WP_Query($args); ?>
          <?php while ($loop->have_posts()): $loop->the_post(); ?>
            <div class="menu-item">
              <h4><?php the_title(); ?></h4>
              <h5><?php the_field('small_price'); ?></h5>
              <h5><?php the_field('large_price'); ?></h5>
              <?php the_content(); ?>
            </div>
          <?php endwhile; wp_reset_postdata(); ?> 
        <?php endforeach; ?>

      </div>
    </div>
  </div>
    
<?php get_footer(); ?>