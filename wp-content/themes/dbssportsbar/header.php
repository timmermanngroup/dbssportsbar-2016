<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package dbssportsbar
 */

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="keywords" content="sports bar st louis, st louis sports bar, st. louis restaurant, sports bar downtown st louis, bar and grill st. louis" />
<meta name="description" content="Come to DB's Sports Bar for awesome food, drinks, and girls- located in downtown St Louis. " />
<title>DB's Sports Bar | Restaurant St Louis</title>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>




<body <?php body_class( ); ?>>
<div class="overlay"></div>
        
    <?php $navbar_class = ( ! empty( get_field('dbs_annoucement_message', 'option') ) ) ? 'accouncement': null ; ?>   
    
    <div class="navbar navbar-default navbar-inverse navbar-static-top navigation <?php echo $navbar_class; ?>">
        
        <?php if (get_field('dbs_annoucement_message', 'option')): ?>
            <?php
            
            $back_color = get_field('dbs_annoucement_background_color', 'option');    
            
            $bar_style  = ( ! empty( $back_color ) ) ? 'style="padding: 1.25em 0 0 0; background-color:' . $back_color .'; "' : null;
            
            ?>

            <div class="announcement-bar" <?php echo $bar_style; ?>>
                <div class="container">
                    <div class="row">
		                <div class="col-md-12">
                            <?php echo '<span class="message">' . get_field('dbs_annoucement_message', 'option') . '</span>'; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="container nav">
            <div class="container top-social hidden-xs hidden-sm text-right">
                <p>Daily DB’s Updates…Click Here to See What is Happening!
                    <?php if (get_field('dbs_facebook', 'option')): ?>
                        <a href="<?php the_field('dbs_facebook', 'option'); ?>" target="_blank"><i class="fa fa-facebook-official fa-fw fa-lg"></i></a>
                    <?php endif; ?>
                    <?php if (get_field('dbs_twitter', 'option')): ?>
                        <a href="<?php the_field('dbs_twitter', 'option'); ?>" target="_blank"><i class="fa fa-twitter fa-fw fa-lg"></i></a>
                    <?php endif; ?>
                    <?php if(!empty(get_field('phone_number', 'option'))): ?>
                        <a itemprop="telephone" href="<?php echo callable_number(get_field('phone_number', 'option')) ?>"><?php the_field('phone_number', 'option') ?></a>
                    <?php endif; ?>
                </p>     
            </div>
            <div class="navbar-header">
                <a href="/"><img class="nav-logo" src="<?php the_field('brand_logo', 'option'); ?>" alt=""></a>
            </div>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span>MENU</span>
            </button>
            <div class="collapse navbar-collapse" id="navbar-ex-collapse">
                <?php wp_nav_menu( array( 'theme_location' => 'main-menu', 'menu_id' => 'main-menu', 'menu_class' => 'nav-right', 'container' => 'nav' ) ); ?>
            </div>
        </div>    
    </div>