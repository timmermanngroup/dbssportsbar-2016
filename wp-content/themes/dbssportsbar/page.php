<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dbssportsbar
 */

get_header(); ?>

<!-- content -->    
  <div class="container interior-content">
	<div class="row">
		<div class="col-md-6">
			<?php
			while ( have_posts() ) : the_post();
				the_content();

			endwhile; // End of the loop.
			?>
		</div>
		<div class="col-md-6">
			<?php if (is_page('Contact')): ?>
				<div class="google-map">
					<?php the_field('map'); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
  </div>


<?php get_footer(); ?>