<!-- contact bar -->    
  <div class="container" id="contact-bar">
    <div class="row">
      <div class="col-md-6" id="contact-left">
        <p>Daily DB’s Updates…Click Here to See What is Happening!
          <?php if (get_field('dbs_facebook', 'option')): ?>
          <a href="<?php the_field('dbs_facebook', 'option'); ?>" target="_blank"><i class="fa fa-facebook-official fa-fw fa-lg"></i></a>
          <?php endif; ?>
          <?php if (get_field('dbs_twitter', 'option')): ?>
          <a href="<?php the_field('dbs_twitter', 'option'); ?>" target="_blank"><i class="fa fa-twitter fa-fw fa-lg"></i></a>
          <?php endif; ?>
        </p>
      </div>
      <div class="col-md-6 text-right" id="contact-right">
        <?php echo do_shortcode( '[gravityform id="3" title="false" description="false"]' ); ?>
      </div>
    </div>
  </div>

<!-- footer -->    
  <footer class="container" id="foot">
    <div class="row">
      <div class="col-sm-12 hidden-xs">
        <?php wp_nav_menu( array( 'theme_location' => 'main-menu', 'menu_id' => 'footer-nav', 'menu_class' => 'foot-nav', 'container' => 'nav' ) ); ?>
      </div>
    </div>  
    <div class="row" itemscope itemtype="http://schema.org/LocalBusiness">
      <div class="col-sm-6 text-center">
        <div class="footer__content_container">
            <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
            <a class="ga-address" href="https://www.google.com/maps/dir//DB's+Sports+Bar,+1615+S+Broadway,+St.+Louis,+MO+63104,+United+States/@38.6094896,-90.2015837,17z/data=!4m12!1m3!3m2!1s0x87d8b3a7129978c9:0xfef6cb0a8a19f0de!2sDB's+Sports+Bar!4m7!1m0!1m5!1m1!1s0x87d8b3a7129978c9:0xfef6cb0a8a19f0de!2m2!1d-90.199395!2d38.6094854?hl=en-US" target="_blank"><span itemprop="streetAddress">1615 S. Broadway</span>, <span itemprop="addressLocality">St Louis</span>, <span itemprop="addressRegion">MO</span> <span itemprop="postalCode">63104</span> US</a>
            </address>

            <?php if(!empty(get_field('phone_number', 'option'))): ?>
                <p><a itemprop="telephone" href="<?php echo callable_number(get_field('phone_number', 'option')) ?>"><?php the_field('phone_number', 'option') ?></a></p>
            <?php endif; ?>
            <p><small>Copyright &copy;<?php echo date('Y'); ?> <span itemprop="name"><?php bloginfo('name'); ?></span>
            <br>Website Design by <a href="http://www.wearetg.com/" target="_blank">Timmermann Group</a></small></p>
        </div>
      </div>

      <div class="col-sm-6">
        <?php get_template_part("template-parts/content", "hours") ?>
      </div>

    </div>
  </footer>
  
<?php wp_footer(); ?>

</body>

</html>