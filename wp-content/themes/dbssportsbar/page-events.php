<?php
/**
 * Template Name: Events Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package dbssportsbar
 */

get_header(); ?>

!-- content -->

<div class="container interior-content">

<?php $args = array('post_type' => 'event') ?>
<?php $loop = new WP_Query($args);?>
<?php $src = get_the_post_thumbnail(); ?>
<?php if ($loop->have_posts()): ?>
<?php while ($loop->have_posts()): $loop->the_post();?>

    <div class="row event-details">
      <div class="col-md-3">
        <?php if ( has_post_thumbnail()): ?>
        <?php the_post_thumbnail('event-image', array( 'class' => 'img_responsive' )); ?>
        <?php endif; ?>
      </div>

      <div class="col-md-9">
         <h3><?php the_title(); ?></h3>
            <div class="event__content">
                <?php the_content(); ?>
            </div>
          <?php if (get_field('event_link')): ?>
            <a class="btn btn-default event__button" data-title="<?php the_title(); ?>" href="<?php the_field('event_link'); ?>" target="_blank">EVENT LINK</a>
          <?php endif; ?>
      </div>
    </div>

<?php endwhile; wp_reset_postdata();?>
<?php endif; ?>

</div>

<?php get_footer(); ?>
