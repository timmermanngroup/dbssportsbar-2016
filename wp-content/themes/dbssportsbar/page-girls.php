<?php
/**
 * Template Name: Girls Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package dbssportsbar
 */

get_header(); ?>

<!-- content -->    
  <div class="container interior-content grid">

    <?php if (have_rows('gallery')): ?>
      <?php while (have_rows('gallery')): the_row(); ?>
        <div class="grid-item">
          <?php $url = wp_get_attachment_url(get_sub_field('gallery_images')); ?>




            <?php if (is_page('Gallery')): ?><a href="<?php echo $url; ?>" rel="lightbox">  <?php endif; ?>
            <img class="img-responsive" src="<?php echo $url; ?>" alt="">
          </a>
        </div>
      <?php endwhile; ?>
    <?php endif; ?>
  </div>

<?php get_footer(); ?>
<script>
  var grid = jQuery('.grid');
  grid.masonry({
    itemSelector: '.grid-item',
  });

  grid.imagesLoaded().progress( function() {
    grid.masonry('layout');
  });
</script>