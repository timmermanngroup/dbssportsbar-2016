<?php
/**
 * Template Name: Contact Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dbssportsbar
 */

get_header(); ?>

<!-- content -->    
  <div class="container interior-content">
	<div class="row">
		<div class="col-md-6">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
		</div>
		<div class="col-md-6">
			<div class="google-map">
				<?php echo get_field('map'); ?>
			</div>
			<div class="contact-info text-center" itemscope itemtype="http://schema.org/Restaurant">
				<meta  itemprop="name" content="<?php bloginfo( 'name' ); ?>">
				<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
					<a class="ga-address" href="https://www.google.com/maps/dir//DB's+Sports+Bar,+1615+S+Broadway,+St.+Louis,+MO+63104,+United+States/@38.6094896,-90.2015837,17z/data=!4m12!1m3!3m2!1s0x87d8b3a7129978c9:0xfef6cb0a8a19f0de!2sDB's+Sports+Bar!4m7!1m0!1m5!1m1!1s0x87d8b3a7129978c9:0xfef6cb0a8a19f0de!2m2!1d-90.199395!2d38.6094854?hl=en-US" target="_blank"><span itemprop="streetAddress">1615 S. Broadway</span>, <span itemprop="addressLocality">St Louis</span>, <span itemprop="addressRegion">MO</span> <span itemprop="postalCode">63104</span></a>
				</p>
				<p><a itemprop="telephone" href="<?php echo callable_number(get_field('phone_number', 'option')) ?>"><?php the_field('phone_number', 'option') ?></a></p>

                <?php get_template_part("template-parts/content", "hours") ?>

			</div>
		</div>
	</div>
  </div>


<?php get_footer(); ?>