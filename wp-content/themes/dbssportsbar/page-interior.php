<?php
/**
 * Template Name: Interior Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package dbssportsbar
 */

get_header(); ?>

<!-- content -->    
  <div class="container interior-content">
    <div class="row">
      <div class="col-md-12">
        
        <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
        <?php the_content(); ?>
        <?php endwhile; ?>
        <?php endif; ?>

        <?php if( have_rows('specials_repeater') ):
            while ( have_rows('specials_repeater') ) : the_row(); ?>                
                <div class="specials">
                  <h1><?php the_sub_field('specials_day'); ?></h1>
                  <h2><?php the_sub_field('specials_special'); ?></h2>
                  <br>
                  <h3><?php the_sub_field('specials_description'); ?></h3>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
        
      </div>
    </div>
  </div>
    
<?php get_footer(); ?>