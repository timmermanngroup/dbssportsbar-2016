<?php
/**
 * Template Name: Front Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package dbssportsbar
 */

get_header(); ?>

<!-- content / cta area -->
<div class="container main-content">
  <div class="row">
      <?php
        while ( have_posts() ) : the_post();
          the_content();
        endwhile;
      ?>
  </div>
</div>

<?php get_footer(); ?>
