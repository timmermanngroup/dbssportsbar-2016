<?php
/**
 * Template Name: Gallery Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package dbssportsbar
 */

get_header(); ?>

<!-- content -->    

  <?php $args = array('post_type' => 'gallery_post_type') ?>
  <?php $loop = new WP_Query($args);?>
  <?php if ($loop->have_posts()): ?>
  <?php while ($loop->have_posts()): $loop->the_post();?>
    
    <div class="container interior-content">
      <div class="row">
        <div class="col-md-12 gallery-title">
          <h2><?php the_title(); ?></h2>
        </div>
      </div>
    </div>

    <div class="container grid">  
      <?php if (have_rows('gallery')): ?>
        <?php while (have_rows('gallery')): the_row(); ?>
          <div class="grid-item">
            <?php $url = wp_get_attachment_url(get_sub_field('gallery_images')); ?>
              <?php if (is_page('Gallery')): ?><a href="<?php echo $url; ?>" rel="lightbox">  <?php endif; ?>
              <img class="img-responsive" src="<?php echo $url; ?>" alt="">
            </a>
          </div>
        <?php endwhile; ?>
      <?php endif; ?>
    </div>
  
  <?php endwhile; wp_reset_postdata();?>
  <?php endif; ?>


<?php get_footer(); ?>
<script>
  var grid = jQuery('.grid');
  grid.masonry({
    itemSelector: '.grid-item',
  });

  grid.imagesLoaded().progress( function() {
    grid.masonry('layout');
  });
</script>