<?php
/**
 * dbssportsbar functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package dbssportsbar
 */

if ( ! function_exists( 'dbssportsbar_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function dbssportsbar_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on dbssportsbar, use a find and replace
	 * to change 'dbssportsbar' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'dbssportsbar', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	//register menus
	function register_my_menu() {
	  register_nav_menu('main-menu',__( 'Main Menu' ));
	  register_nav_menu('footer-nav',__( 'Footer Nav' ));
	}
	add_action( 'init', 'register_my_menu' );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'dbssportsbar_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // dbssportsbar_setup
add_action( 'after_setup_theme', 'dbssportsbar_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function dbssportsbar_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'dbssportsbar_content_width', 640 );
}
add_action( 'after_setup_theme', 'dbssportsbar_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function dbssportsbar_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'dbssportsbar' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'dbssportsbar_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function dbssportsbar_scripts() {
	wp_deregister_script('jquery'); //prevents jquery from loading in the head
	//wp_enqueue_style( 'dbssportsbar-style', get_stylesheet_uri() );

	// if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
	// 	wp_enqueue_script( 'comment-reply' );
	// }

	wp_register_script( 'masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js' , 'jquery', '1.4.1', true );

	wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js', array(), '1.11.2', true );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '3.5.5', true );
	wp_enqueue_script( 'smooth-scroll', get_template_directory_uri() . '/js/smooth-scroll.js', array(), '', true );
	// wp_enqueue_script( 'ga', get_template_directory_uri() . '/js/ga.js', array('jquery'), '', true );
	//wp_enqueue_style( 'oswald', 'https://fonts.googleapis.com/css?family=Oswald:400,700,300' );
	wp_enqueue_style( 'dbssportsbar', get_template_directory_uri() . '/css/dbssportsbar.css' );
	wp_enqueue_script( 'modernizer', get_template_directory_uri() . '/js/modernizer-2.8.3.js' , array(), '', true );

	if( is_page_template( 'page-gallery.php' ) || is_page_template( 'page-girls.php' ) ) {
		wp_enqueue_script( 'masonry' );
	}

}
add_action( 'wp_enqueue_scripts', 'dbssportsbar_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load Custom Post Types.
 */
require get_template_directory() . '/inc/custom-post-types.php';


// SVG allow
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// ACF options
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'DBs Options',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

// Register Custom Post Type
function event_post_type() {

	$labels = array(
		'name'                  => _x( 'Events', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Event', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Events', 'text_domain' ),
		'name_admin_bar'        => __( 'Events', 'text_domain' ),
		'archives'              => __( 'Event Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Event:', 'text_domain' ),
		'all_items'             => __( 'All Events', 'text_domain' ),
		'add_new_item'          => __( 'Add New Event', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Event', 'text_domain' ),
		'edit_item'             => __( 'Edit Event', 'text_domain' ),
		'update_item'           => __( 'Update Event', 'text_domain' ),
		'view_item'             => __( 'View Event', 'text_domain' ),
		'search_items'          => __( 'Search Events', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Event', 'text_domain' ),
		'description'           => __( 'News & Events', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => false,
		'capability_type'       => 'page',
	);
	register_post_type( 'event', $args );

}
add_action( 'init', 'event_post_type', 0 );

/*
** Add Favicons
*/
function dbs_favicons() {
	$color = '#000'; //Change this hex value to whatever you would like
	$template_path = get_template_directory_uri().'/img/favicons';
	echo '<link rel="apple-touch-icon" sizes="57x57" href="' . $template_path . '/apple-touch-icon-57x57.png">';
	echo '<link rel="apple-touch-icon" sizes="60x60" href="' . $template_path . '/apple-touch-icon-60x60.png">';
	echo '<link rel="apple-touch-icon" sizes="72x72" href="' . $template_path . '/apple-touch-icon-72x72.png">';
	echo '<link rel="apple-touch-icon" sizes="76x76" href="' . $template_path . '/apple-touch-icon-76x76.png">';
	echo '<link rel="apple-touch-icon" sizes="114x114" href="' . $template_path . '/apple-touch-icon-114x114.png">';
	echo '<link rel="apple-touch-icon" sizes="120x120" href="' . $template_path . '/apple-touch-icon-120x120.png">';
	echo '<link rel="apple-touch-icon" sizes="144x144" href="' . $template_path . '/apple-touch-icon-144x144.png">';
	echo '<link rel="apple-touch-icon" sizes="152x152" href="' . $template_path . '/apple-touch-icon-152x152.png">';
	echo '<link rel="apple-touch-icon" sizes="180x180" href="' . $template_path . '/apple-touch-icon-180x180.png">';
	echo '<link rel="icon" type="image/png" href="' . $template_path . '/favicon-32x32.png" sizes="32x32">';
	echo '<link rel="icon" type="image/png" href="' . $template_path . '/favicon-194x194.png" sizes="194x194">';
	echo '<link rel="icon" type="image/png" href="' . $template_path . '/favicon-96x96.png" sizes="96x96">';
	echo '<link rel="icon" type="image/png" href="' . $template_path . '/android-chrome-192x192.png" sizes="192x192">';
	echo '<link rel="icon" type="image/png" href="' . $template_path . '/favicon-16x16.png" sizes="16x16">';
	echo '<link rel="manifest" href="' . $template_path . '/manifest.json">';
	echo '<link rel="mask-icon" href="' . $template_path . '/safari-pinned-tab.svg" color="#000">';
	echo '<link rel="shortcut icon" href="' . $template_path . '/favicon.ico">';
	echo '<meta name="msapplication-TileColor" content="#000">';
	echo '<meta name="msapplication-TileImage" content="' . $template_path . '/mstile-144x144.png">';
	echo '<meta name="msapplication-config" content="' . $template_path . '/browserconfig.xml">';
	echo '<meta name="theme-color" content="#000">';
}
add_action( 'wp_head', 'dbs_favicons' );

add_filter( 'gform_tabindex_3', '__return_false' );

if(!function_exists("callable_number")) { 
    function callable_number($dirty_number, $tel_prefix = true, $country_code = 1) { // -> String
        $clean_number = preg_replace('/[^0-9]/', '', $dirty_number);
        if($tel_prefix == true) { 
            return "tel:+".$country_code.$clean_number;
        } else { 
            return $clean_number;
        }
    }
}